// include the library code:
#include <Wire.h>
#include <SerLCD.h> // OpenLCD Click here to get the library: http://librarymanager/All#SparkFun_SerLCD
#include <SHT1x.h> // https://www.seeedstudio.com/Soil-Moisture-Temperature-Sensor-p-1356.html

#define ENABLED_PWR false
#define ENABLED_ENCODER true
#define ENABLED_BTN1 false
#define ENABLED_BTN2 false
#define ENABLED_WEIGHT false
#define ENABLED_TH_SENSOR true
#define ENABLED_FAN false
#define ENABLED_LCD true

//encoder pins
#define ENC_PIN2 12
#define ENC_PIN1 11

//relay pins
#define PWR_HUM 7
#define PWR_DEHUM 9
#define PWR_FRIDGE 10
#define PWR_FAN 2
#define BTN_SET_1 19
#define BTN_SET_2 20
#define WGT_PIN 21

// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
SerLCD lcd; // Initialize the library with default I2C address 0x72

/*
# D7 = pin 7 = Humidifier
# D9 = pin 9 = Dehumidifier
# D10 = pin 10 = Refrigerator
# D11/D12 = pins 11/12 = encoder
# A1 = AIN2 = = pin 5? or pin 10? = button 1
# A2 = AIN3 = pin 8? = button 2
# A3 = Weight sensor
# A4/A5 = Temp/Humidity sensor
# D2 = fan control
# SCL/SDA = I2C (LCD Display)
*/
/*
A0 = 14
A1 = 15
A2 = 16
A3 = 17
A4 = 18
A5 = 19
*/

SHT1x sht1x(A4, A5); //initialize hum/temp sensor
uint8_t i=0;
int val1 = 0, val2 = 0;
int sensorValue = 0;
byte downarrow[8] = {0x0,0x4,0x4,0x4,0x15,0xe,0x4,0x0};
volatile int buttonState = 0;         // variable for reading the pushbutton status
volatile byte INTFLAG1 = 0; // interrupt status flag

volatile unsigned int master_count = 0; // encoder count
byte whichLine = 0;
volatile unsigned long last_interrupt_time = 0;
unsigned long lastUpdateTime = 0;

void setup() {
  // Debugging output
  Serial.begin(9600);
  // set up the LCD's number of columns and rows: 
  if(ENABLED_LCD){
    Wire.begin();
    lcd.begin(Wire); //Set up the LCD for I2C communication
    delay(50);
    lcd.createChar(0,downarrow);
    lcd.disableSystemMessages();
    delay(50);
    lcd.setFastBacklight(0xFFFFFF); //Set backlight to bright white
    delay(50);
    lcd.setContrast(0x10);
    delay(50);
    lcd.noAutoscroll();
    delay(50);
    
    lcd.clear(); //Clear the display - this moves the cursor to home position as well
    
    lcd.print("Hello, wold!");
  }

  //init button pins regardless if we're using them
  pinMode(A1, INPUT);
  //pinMode(A2, INPUT);
  
  //initialize the encoder
  if(ENABLED_ENCODER){
    pinMode(CHA, INPUT_PULLUP);
    pinMode(CHB, INPUT_PULLUP);

    //interrupt channels are same as pin numbers for this board
    attachInterrupt(CHB, encoder_ISR_B, CHANGE);
    attachInterrupt(CHA,encoder_ISR_A, CHANGE);
  }
  
  //turn the fan on
  if(ENABLED_FAN){
    pinMode(PWR_FAN,OUTPUT);
    digitalWrite(PWR_FAN,1);
  }
  if(ENABLED_PWR){
    pinMode(PWR_FRIDGE,OUTPUT);
    pinMode(PWR_HUM,OUTPUT);
    pinMode(PWR_DEHUM,OUTPUT);    
  }
}

/*
//OLD CODE - SAVING IN CASE I WANT TO LOOK BACK
void pin_ISR_UP() {
  buttonState = 0;
  //digitalWrite(ledPin, buttonState);
}

void pin_ISR_DOWN() {
  unsigned long interrupt_time = millis();
   // If interrupts come faster than 200ms, assume it's a bounce and ignore
   if (interrupt_time - last_interrupt_time > 150) 
   {
      last_interrupt_time = interrupt_time;
      buttonState += 1;
    }
 }
*/

int printLCD(String text){
  if(text.length() > 16){
    return lcd.print(text.substring(0,16));
  }
  else{
    return lcd.print(text);
  }
}

unsigned int lastReportedPos = 1;   // change management
static boolean rotating = false;    // debounce management
boolean A_set = false;
boolean B_set = false;

void encoder_ISR_A() {
   // debounce
  //if ( rotating ) delay (1);  // wait a little until the bouncing is done

  // Test transition, did things really change?
  if ( digitalRead(CHA) != A_set ) { // debounce once more
    A_set = !A_set;

    // adjust counter + if A leads B
    if ( A_set && !B_set )
      master_count += 1;

    rotating = false;  // no more debouncing until loop() hits again
  }
}

// Interrupt on B changing state, same as A above
void encoder_ISR_B() {
  //if ( rotating ) delay (1);
  if ( digitalRead(CHB) != B_set ) {
    B_set = !B_set;
    //  adjust counter - 1 if B leads A
    if ( B_set && !A_set )
      master_count -= 1;

    rotating = false;
  }
}

void writeLineOne(){
  String lineOne = "B1: {0} B2: {1} ";
  if(ENABLED_BTN1){
    analogReadResolution(10);
    val1 = analogRead(A1);
    //lineOne.replace("{0}",val1==0 ? "ON " : "OFF");
    lineOne.replace("{0}",String(val1,DEC));
  } else {
    lineOne.replace("{0}","NA ");
  }
  /*
  if(ENABLED_BTN2){
    val2 = digitalRead(A2);
    lineOne.replace("{1}",val2==0 ? "ON " : "OFF");
  } else {
    lineOne.replace("{1}","NA ");
  }
  */
  //analogReadResolution(10);
  //val2 = pulseIn(A2, LOW);
  val2 = analogRead(A1);
  float volt1 = val2 * (3.3 / 1024.0);
  lineOne.replace("{1}",String(volt1,2));
  if(ENABLED_LCD){
      lcd.setCursor(0,0);
      printLCD(lineOne); 
  }
}

void writeLineTwo(){
  String lineTwo = "Wgt:{2} Enc:{3}  ";
  //String lineTwo = "LINE 2 Test     ";

  if(ENABLED_WEIGHT){
    sensorValue = analogRead(A3); // weight value
    lineTwo.replace("{2}",String(sensorValue,3));
  } else {
    lineTwo.replace("{2}","NA");
  }
  if(ENABLED_ENCODER){
    lineTwo.replace("{3}",String(master_count,DEC));
  } else {
    lineTwo.replace("{3}","NA");
  }
 
  if(ENABLED_LCD){
      lcd.setCursor(0, 1);
      //delay(20);
      lcd.print(lineTwo); 
  }
}

void writeLineThree(){
  //String lineThree = "{4}\xDF""F  Hum: {5}%   ";
  String lineThree = "{4}F  Hum: {5}%   ";
  if(ENABLED_TH_SENSOR){
    //float temp_c;
    float temp_f;
    float humidity;
    //temp_c = sht1x.readTemperatureC();
    temp_f = sht1x.readTemperatureF();
    humidity = sht1x.readHumidity();
    if(temp_f <= 0.0 || humidity <= 0.0){
       lineThree = "TH SNSR OFFLN";
    } else {
      lineThree.replace("{4}",String(temp_f,1));
      lineThree.replace("{5}",String(humidity,DEC));
    }
  } else {
    lineThree = "TH SNSR OFFLN   ";
  }
    if(ENABLED_LCD){
      lcd.setCursor(0, 1);
      //delay(20);
      lcd.print(lineThree); 
  }
}

void writeLineFour(){
  String lineFour = "Fr:{0} H:{1} D:{2} Fa:{3}";
  if(bitRead(PORTD,PWR_FRIDGE)){ //fridge is on
    lineFour.replace("{0}","+"); 
  } else {
    lineFour.replace("{0}","+"); 
  }

  if(bitRead(PORTD,PWR_HUM)){ //humidifier is on
    lineFour.replace("{1}","+"); 
  } else {
    lineFour.replace("{1}","+"); 
  }

  if(bitRead(PORTD,PWR_DEHUM)){ //dehumidifier is on
    lineFour.replace("{2}","+"); 
  } else {
    lineFour.replace("{2}","+"); 
  }

  if(bitRead(PORTD,PWR_FAN)){ //fan is on
    lineFour.replace("{3}","+"); 
  } else {
    lineFour.replace("{3}","+"); 
  }
}

void writething(){
  String test = " 1 2 3 4 5 6 7 8";
  test[master_count % 8 * 2] = (char)0x0;
  lcd.setCursor(0,1);
  lcd.print(test);
}
   
void loop() {
  /*if (lastReportedPos != master_count) {
    Serial.print("Index:");
    Serial.println(master_count, DEC);
    lastReportedPos = master_count;
  }*/
  writeMainSettings()
  writeLineThree();
  writeLineOne();
  
  if(millis() - lastUpdateTime > 2000){
    if(whichLine){
      lcd.setFastBacklight(0xFFFFFF); //Set backlight to bright white
//      lcd.setContrast(0x10);
      //Serial.println(lineThree);
      writeLineThree();
      writeLineTwo();
      whichLine = 0;
    } else {
      lcd.setFastBacklight(0xFFFFFF); //Set backlight to bright white
  //    lcd.setContrast(0x10);
      writeLineTwo();
      whichLine = 1;
    }
    lastUpdateTime = millis();
  }
 // writething();
  
  if(ENABLED_PWR){
    float temp_f = sht1x.readTemperatureF();
    if(temp_f > 55.0){
      digitalWrite(PWR_FRIDGE,HIGH);
    } else if (temp_f < 52.0){
      digitalWrite(PWR_FRIDGE,LOW);
    }
    float humidity;
    humidity = sht1x.readHumidity();

    //TODO: rewrite this to keep hum/dehum on until 72.5% if on.
    if(humidity > 75.0){ //humidity is above 75% - turn on dehumidifier
      digitalWrite(PWR_DEHUM,HIGH);
      digitalWrite(PWR_HUM,LOW);
    } else if(humidity < 70.0){ //humidity is below 70% - turn on humidifier
      digitalWrite(PWR_HUM,HIGH);
      digitalWrite(PWR_DEHUM,LOW);
    } else {
      digitalWrite(PWR_HUM,LOW);
      digitalWrite(PWR_DEHUM,LOW);
    }
  }
  
}

void writeMainSettings(int topLvlVal){
  String lineOne = "";
  //String lineTwo = "Wgt:{2} Enc:{3}  ";
  topLvlVal = topLvlVal % 5;
  switch (topLvlVal)
  {
    case 0:
      if(IS_RUNNING)
        lineOne = "1. STOP PROGRAM";
      else
        lineOne = "1. START PROGRAM";
      break;
    case 1:
      lineOne = "2. HOLD TEMP/RH";
      break;
    case 2:
      lineOne = "3. SKIP TO STAGE";
      break;
    case 3:
      lineOne = "4. EDIT PROGRAM";
      break;
    case 4:
      lineOne = "Calibrate System";
      break;
    default:
      break;
  }


  if(ENABLED_LCD){
      lcd.setCursor(0,0);
      printLCD(lineOne); 
      lcd.setCursor(0,1);
      printLCD("                ");
  }
}