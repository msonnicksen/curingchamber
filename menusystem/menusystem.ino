// include the library code:
#include <Wire.h>
#include <SerLCD.h> // OpenLCD Click here to get the library: http://librarymanager/All#SparkFun_SerLCD
#include <SPI.h>
#include <Adafruit_SPIFlash.h>
#include <Adafruit_SPIFlash_FatFs.h>
#include <LinkedList.h>

// Configuration of the flash chip pins and flash fatfs object.
// You don't normally need to change these if using a Feather/Metro
// M0 express board.
#define FLASH_TYPE     SPIFLASHTYPE_W25Q16BV  // Flash chip type.
                                              // If you change this be
                                              // sure to change the fatfs
                                              // object type below to match.

#if defined(__SAMD51__)
  // Alternatively you can define and use non-SPI pins, QSPI isnt on a sercom
  Adafruit_SPIFlash flash(PIN_QSPI_SCK, PIN_QSPI_IO1, PIN_QSPI_IO0, PIN_QSPI_CS);
#else
  #if (SPI_INTERFACES_COUNT == 1)
    #define FLASH_SS       SS                    // Flash chip SS pin.
    #define FLASH_SPI_PORT SPI                   // What SPI port is Flash on?
  #else
    #define FLASH_SS       SS1                    // Flash chip SS pin.
    #define FLASH_SPI_PORT SPI1                   // What SPI port is Flash on?
  #endif

Adafruit_SPIFlash flash(FLASH_SS, &FLASH_SPI_PORT);     // Use hardware SPI
#endif
// Finally create an Adafruit_M0_Express_CircuitPython object which gives
// an SD card-like interface to interacting with files stored in CircuitPython's
// flash filesystem.
Adafruit_M0_Express_CircuitPython pythonfs(flash);

#define ENABLED_LCD true
#define ENABLED_ENCODER true
#define ENABLED_WEIGHT false
// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
SerLCD lcd; // Initialize the library with default I2C address 0x72

uint8_t i = 0;
#define ENABLED_PWR false
#define ENABLED_ENCODER true
#define ENABLED_BTN1 false
#define ENABLED_BTN2 false
#define ENABLED_WEIGHT false
#define ENABLED_TH_SENSOR true
#define ENABLED_FAN false
#define ENABLED_LCD true

//encoder pins
#define ENC_PIN2 11
#define ENC_PIN1 12

//relay pins
#define PWR_HUM 7
#define PWR_DEHUM 9
#define PWR_FRIDGE 10
#define PWR_FAN 2
#define BTN_SET_1 19
#define BTN_SET_2 20
#define WGT_PIN 21
#define FAN_MODE_OFF 0x01
#define FAN_MODE_ON 0x02
#define FAN_MODE_AUTO 0x00

volatile int buttonState = 0; // variable for reading the pushbutton status
volatile byte INTFLAG1 = 0;   // interrupt status flag
volatile byte buttonsPressed = 0;
volatile unsigned long last_interrupt_time = 0;

String incomingByte;
int buttons1 = 0;
int buttons2 = 0;
byte lastButtonsPressed = 0;
volatile int master_count = 0; // encoder count
int old_master_count = 0;

byte arrowLeft[8] = { 0x00,  0x00,  0x04,  0x08,  0x1F,  0x08,  0x04,  0x00 };
byte arrowRight[8] = { 0x0, 0x8, 0xc, 0xe, 0xc, 0x8, 0x0, 0x0 };

byte whichLine = 0;
unsigned long lastUpdateTime = 0;
int currentMenuState = 0;

//These will be the running values;
double setTemp = 60.0;
double setRH = 50.0;
bool setFan = false;

struct ProgramSetting {
  double temperature;
  double humidity;
  int durationHours;
  byte fanMode; //FAN_MODE_AUTO, FAN_MODE_OFF, or FAN_MODE_ON
};

LinkedList<ProgramSetting> *programList = new LinkedList<ProgramSetting>();

void setup()
{
  // Debugging output
  Serial.begin(115200);
  // set up the LCD's number of columns and rows:
  Wire.begin();
  lcd.begin(Wire); //Set up the LCD for I2C communication
  delay(50);

  lcd.disableSystemMessages();
  delay(50);
  //lcd.setFastBacklight(0xFFFFFF); //Set backlight to bright white
  delay(50);
  lcd.setContrast(0x10);
  delay(50);
  lcd.noAutoscroll();
  delay(50);
  lcd.createChar(0, arrowLeft);
  lcd.createChar(1, arrowRight);

  lcd.clear(); //Clear the display - this moves the cursor to home position as well
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);

  if (ENABLED_ENCODER)
  {
    pinMode(ENC_PIN2, INPUT_PULLUP);
    pinMode(ENC_PIN1, INPUT_PULLUP);

    //interrupt channels are same as pin numbers for this board
    attachInterrupt(ENC_PIN2, encoder_ISR_B, CHANGE);
    attachInterrupt(ENC_PIN1, encoder_ISR_A, CHANGE);
  }
  //attachInterrupt(A1, pin_BTN_PRESS, RISING);
  //attachInterrupt(A2, pin_BTN_PRESS, RISING);
  //lcd.print("Hello, world!");

  //initialize flash
  if (!flash.begin(FLASH_TYPE)) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1);
  }
  //initialize filesystem
  if (!pythonfs.begin()) {
    Serial.println("Failed to mount filesystem!");
    Serial.println("Was CircuitPython loaded on the board first to create the filesystem?");
    while(1);
  }
  loadConfig();
  //loadPreviousRunningValues();
}

unsigned int lastReportedPos = 1; // change management
int menuDepth = 0;
int menuPointer[] = {0, 0, 0, 0, 0, 0};
volatile bool pressed = false;

void pin_BTN_PRESS()
{

  int val1 = analogRead(A1);
  int val2 = analogRead(A2);
  float volt1 = val1 * (3.3 / 1024.0);
  float volt2 = val2 * (3.3 / 1024.0);
  pressed = true;
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  //if (interrupt_time - last_interrupt_time > 150)
  //{
  Serial.println("volt 1: " + String(volt1, 2));
  Serial.println("volt 2: " + String(volt2, 2));
  //last_interrupt_time = interrupt_time;
  if (1.5 < volt1 && volt1 < 1.8)
  {
    buttonsPressed |= 1UL << 1;
    Serial.println("pressed 1");
  }
  else if (volt1 > 1.8)
  {
    buttonsPressed |= 1UL << 2;
    Serial.println("pressed 2");
  }
  //last_interrupt_time = interrupt_time;
  if (1.5f < volt2 && volt2 < 1.8)
  {
    buttonsPressed |= 1UL << 3;
    Serial.println("pressed 3");
  }
  else if (volt2 > 1.8)
  {
    buttonsPressed |= 1UL << 4;
    Serial.println("pressed 4");
  }
}

bool IS_RUNNING = false;
void writeMainSettings(int topLvlVal)
{
  String lineOne = "";
  //String lineTwo = "Wgt:{2} Enc:{3}  ";
  topLvlVal = topLvlVal % 5;
  switch (topLvlVal)
  {
  case 0:
    if (IS_RUNNING)
      lineOne = "1. STOP PROGRAM   ";
    else
      lineOne = "1. START PROGRAM";
    break;
  case 1:
  case -4:
    lineOne = "2. HOLD TEMP/RH ";
    break;
  case 2:
  case -3:
    lineOne = "3. SKIP TO STAGE  ";
    break;
  case 3:
  case -2:
    lineOne = "4. EDIT PROGRAM    ";
    break;
  case 4:
  case -1:
    lineOne = "Calibrate System";
    break;
  default:
    break;
  }

  if (ENABLED_LCD)
  {
    lcd.setCursor(0, 0);
    printLCD(lineOne);
    lcd.setCursor(0, 1);
    printLCD("                ");
  }
}

void writeSecondLevelSettings(int topLevelVal, int twoLevelVal, int adjustment)
{
  if (topLevelVal == 0 && !IS_RUNNING) //1: start program. there is no 2nd level settings, go back to main level.
  {
    startProgram();
  }
  //Hold Temp/RN
  if (topLevelVal == 1)
  {
      menuHoldTemp(twoLevelVal, adjustment);
  } else if(topLevelVal == 2){
      menuSkipToStage(twoLevelVal, adjustment);
  } else if(topLevelVal == 3){
      menuEditProgram(twoLevelVal, adjustment);
  }
}

void writeThirdLevelSettings(int topLevelVal, int twoLevelVal, int threeLevelVal)
{
  if(topLevelVal == 3 && twoLevelVal == 0){
    menuAddStage(threeLevelVal);
  }
}


int printLCD(String text)
{
  if (text.length() > 16)
  {
    return lcd.print(text.substring(0, 16));
  }
  else
  {
    return lcd.print(text);
  }
}

void loop()
{
  pin_BTN_PRESS();
  /*switch (menuDepth)
  {
    case 0:
      writeMainSettings(menuDepthValues[0]);
      break;
    case 1:
      writeSecondLevelSettings(menuDepthValues[0],menuDepthValues[1]);
      break;
    default:
      //writeLineOne();
      break;
  }*/
  //lcd.setCursor(0,0);
  //  printLCD("BUTTON 1: " + String(buttons1,DEC));
  //  lcd.setCursor(0,1);
  //  printLCD("BUTTON 2: " + String(buttons2,DEC));

  // printLCD("BUTTON 2: " + String(volt2,2));
  //printLCD("Pressed? " + String(buttons1,DEC)  + ":" + String(buttons2,DEC));

  if (buttonsPressed > 0x0 && lastButtonsPressed != buttonsPressed)
  {
    if ((buttonsPressed >> 1) & 1U) //button up is pressed
      menuPointer[menuDepth] -= 1;
    else if ((buttonsPressed >> 2) & 1U) //button down is pressed
      menuPointer[menuDepth] += 1;
    if ((buttonsPressed >> 4) & 1U) //back button is pressed
    {
      menuPointer[menuDepth] = 0;
      menuDepth -= 1;
      master_count = 0;
      if (menuDepth < 0)
        menuDepth = 0;
    }
    else if ((buttonsPressed >> 3) & 1U) // enter button is pressed
    {
      menuDepth += 1;
      menuPointer[menuDepth] = 0;
      if (menuDepth > 3)
        menuDepth = 3;
    }
    last_interrupt_time = millis();
  }
  else if (buttonsPressed > 0) // if we're continuing to hold a button...
  {
    if (((buttonsPressed >> 3) & 1U)) //and that button is the enter button...
    {
      if(last_interrupt_time + 2000 < millis()){ //and we held the button for 2 or more seconds, do things based on menu position/depth
        menuDepth -= 1;
        if (menuDepth == 0 && IS_RUNNING) // 1: stop program
        {
          stopProgram();
        } 
        else if(menuDepth == 1 && (menuPointer[0] % 5 == 1 || menuPointer[0] % 5 == -4)){ //2: hold values
          lcd.setCursor(0,0);
          printLCD("VALUES SAVED");
          delay(1500);
          menuDepth = 0;
        } 
        else {
          lcd.setCursor(0,0);
          printLCD("D:" + String(menuDepth,DEC) + " P:" + String(menuPointer[0],DEC) + "          ");
          delay(1500);
          menuDepth = 0;
        }
      }
    }
    
  }
  
  
  if(lastButtonsPressed != buttonsPressed || old_master_count != master_count){
    if (menuDepth == 0)
      writeMainSettings(menuPointer[0]);
    else if (menuDepth == 1)
      writeSecondLevelSettings(menuPointer[0], menuPointer[1], 0);
    else if(menuDepth == 2 ) {
      writeThirdLevelSettings(menuPointer[0], menuPointer[1], menuPointer[2]);
    }
  }
  lastButtonsPressed = buttonsPressed;
  buttonsPressed = 0;
  old_master_count = master_count;
}

void stopProgram(){
    lcd.setCursor(0, 0);
    printLCD("STOPPING PROGRAM");
    lcd.setCursor(0, 1);
    printLCD(".               ");
    lcd.setCursor(1, 1);
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
    IS_RUNNING = false;
    // buttonsPressed = 0;
    menuDepth = 0;
}

void startProgram(){
    IS_RUNNING = true;
    menuDepth = 0;
    lcd.setCursor(0, 0);
    printLCD("STARTING PROGRAM");
    lcd.setCursor(0, 1);
    printLCD(".               ");
    lcd.setCursor(1, 1);
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
    delay(300);
    printLCD(".");
}

void menuHoldTemp(int twoLevelVal, int adjustment){
  //Adjust Temp, RN, and Fan depending on what row is selected.
    if (master_count != 0)
    {
      if (twoLevelVal % 3 == 0)
      {
        setTemp += (0.1 * master_count);
      }
      else if (twoLevelVal % 3 == 1)
      {
        setRH += (0.1 * master_count);
      }
      else if(twoLevelVal % 3 == 2){
        setFan = !setFan;
      }
      master_count = 0;
    }

    int modVal = (twoLevelVal % 3 + 3) % 3;
    //Display Setting Values
    if(modVal != 1){
      lcd.setCursor(0, modVal / 2);
      String str_setTemp = String(setTemp, 1);
      str_setTemp = String(4 - str_setTemp.length(), '0');
      printLCD("TEMP: " + str_setTemp + "     ");
    }
    if(modVal != 2){
      String str_setRH = String(setRH, 1);
      str_setRH = String(4 - str_setRH.length(), '0');

      lcd.setCursor(0, 1 - modVal);
      printLCD("  RH: " + str_setRH + "    ");
    }
    if(modVal != 0){
      String str_setRH = String(setRH, 1);
      str_setRH = String(4 - str_setRH.length(), '0');

      lcd.setCursor(0, 2 - modVal);
      if(setFan)
        printLCD(" FAN: ON       ");
      else
        printLCD(" FAN: OFF      ");
    }

    lcd.setCursor(15, 0);
    lcd.writeChar(1);
    lcd.setCursor(15, 1);
    printLCD(" ");
}

void menuSkipToStage(int twoLevelVal, int adjustment){
  if(programList->size() == 0){
    lcd.clear();
    lcd.setCursor(0,0);
    printLCD("No Stages Avail");
    delay(2000);
    menuDepth = 0;
    return;
  }
    lcd.clear();
    lcd.setCursor(0,0);
    printLCD("Stage ct: ");
    printLCD(String(programList->size(),DEC));
    delay(2000);
    menuDepth = 0;
}

void menuEditProgram(int twoLevelVal, int adjustment){
  String lineOne = "";
  String lineTwo = "                ";
  twoLevelVal = twoLevelVal % 3;

  if(programList->size() == 0)
      twoLevelVal = 0;
  switch (twoLevelVal)
  {
    case 0:
      lineOne = "1. ADD STAGE    ";
      break;
    case 1:
    case -3:
      lineOne = "2. EDIT STAGE   ";
      break;
    case 2:
    case -2:
      lineOne = "3. DELETE STAGE ";
      break;
    case 3:
    case -1:
      lineOne = "4. CLEAR        ";
      lineTwo = "   ALL STAGES   ";
      break;
    default:
      break;
  }

  if (ENABLED_LCD)
  {
    lcd.setCursor(0, 0);
    printLCD(lineOne);
    lcd.setCursor(0, 1);
    printLCD(lineTwo);
  }

}

void menuAddStage(int thirdVal){
  ProgramSetting setting;
  setting.temperature = 80;
  setting.humidity = 72;
  setting.durationHours = 72;
  setting.fanMode = FAN_MODE_AUTO;
  programList->add(setting);
  menuDepth = 0;
}

void loadConfig(){
    // Check if a boot.py exists and print it out.
  if (pythonfs.exists("system.conf")) {
    File bootPy = pythonfs.open("system.conf", FILE_READ);
    
    while (bootPy.available()) {
      char c = bootPy.read();
      Serial.print(c);
    }
    Serial.println();
  }
  else {
    File data = pythonfs.open("system.conf", FILE_WRITE | FA_CREATE_NEW);
    if(data){
      data.close();
    }
  }
}

void writeConfig(){

}

void logToFile(){
  File dataFile = pythonfs.open("system.log", FILE_WRITE);
  // Check that the file opened successfully and write a line to it.
  if (dataFile) {
    // Take a new data reading from a sensor, etc.  For this example just
    // make up a random number.
    int reading = random(0,100);
    // Write a line to the file.  You can use all the same print functions
    // as if you're writing to the serial monitor.  For example to write
    // two CSV (commas separated) values:

    dataFile.print(millis(), DEC);
    dataFile.print(",");
    dataFile.print(reading, DEC);
    dataFile.println();
    // Finally close the file when done writing.  This is smart to do to make
    // sure all the data is written to the file.
    dataFile.close();
}
}

static boolean rotating = false; // debounce management
boolean A_set = false;
boolean B_set = false;

void encoder_ISR_A()
{
  // debounce
  //if ( rotating ) delay (1);  // wait a little until the bouncing is done

  // Test transition, did things really change?
  if (digitalRead(ENC_PIN1) != A_set)
  { // debounce once more
    A_set = !A_set;

    // adjust counter + if A leads B
    if (A_set && !B_set)
      master_count += 1;

    rotating = false; // no more debouncing until loop() hits again
  }
}

// Interrupt on B changing state, same as A above
void encoder_ISR_B()
{
  //if ( rotating ) delay (1);
  if (digitalRead(ENC_PIN2) != B_set)
  {
    B_set = !B_set;
    //  adjust counter - 1 if B leads A
    if (B_set && !A_set)
      master_count -= 1;

    rotating = false;
  }
}
