// include the library code:
#include <Wire.h>
#include <SerLCD.h> // OpenLCD Click here to get the library: http://librarymanager/All#SparkFun_SerLCD
#include <SHT1x.h> // https://www.seeedstudio.com/Soil-Moisture-Temperature-Sensor-p-1356.html
#include <SPI.h>
#include <Adafruit_SPIFlash.h>
#include <Adafruit_SPIFlash_FatFs.h>

// Configuration of the flash chip pins and flash fatfs object.
// You don't normally need to change these if using a Feather/Metro
// M0 express board.
#define FLASH_TYPE     SPIFLASHTYPE_W25Q16BV  // Flash chip type.
                                              // If you change this be
                                              // sure to change the fatfs
                                              // object type below to match.

#if defined(__SAMD51__)
  // Alternatively you can define and use non-SPI pins, QSPI isnt on a sercom
  Adafruit_SPIFlash flash(PIN_QSPI_SCK, PIN_QSPI_IO1, PIN_QSPI_IO0, PIN_QSPI_CS);
#else
  #if (SPI_INTERFACES_COUNT == 1)
    #define FLASH_SS       SS                    // Flash chip SS pin.
    #define FLASH_SPI_PORT SPI                   // What SPI port is Flash on?
  #else
    #define FLASH_SS       SS1                    // Flash chip SS pin.
    #define FLASH_SPI_PORT SPI1                   // What SPI port is Flash on?
  #endif

Adafruit_SPIFlash flash(FLASH_SS, &FLASH_SPI_PORT);     // Use hardware SPI
#endif
// Finally create an Adafruit_M0_Express_CircuitPython object which gives
// an SD card-like interface to interacting with files stored in CircuitPython's
// flash filesystem.
Adafruit_M0_Express_CircuitPython pythonfs(flash);


// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
SerLCD lcd; // Initialize the library with default I2C address 0x72

uint8_t i = 0;
#define ENABLED_PWR true
#define ENABLED_HUM true
#define ENABLED_DEHUM false
#define ENABLED_FRIDGE true
#define ENABLED_ENCODER true
#define ENABLED_BTN1 true
#define ENABLED_BTN2 true
#define ENABLED_WEIGHT false
#define ENABLED_TH_SENSOR true
#define ENABLED_FAN false
#define ENABLED_LCD true
#define HALF_MINUTE 30000
#define MINUTE 60000
//encoder pins
#define ENC_PIN2 11
#define ENC_PIN1 12

//relay pins
#define PWR_HUM 9
#define PWR_DEHUM 7
#define PWR_FRIDGE 10
#define PWR_FAN 2
#define BTN_SET_1 19
#define BTN_SET_2 20
#define WGT_PIN 21
#define FAN_MODE_OFF 0x01
#define FAN_MODE_ON 0x02
#define FAN_MODE_AUTO 0x00

volatile int buttonState = 0; // variable for reading the pushbutton status
volatile byte INTFLAG1 = 0;   // interrupt status flag
volatile byte buttonsPressed = 0;
volatile unsigned long last_interrupt_time = 0;

String incomingByte;
int buttons1 = 0;
int buttons2 = 0;
byte lastButtonsPressed = 0;
volatile int master_count = 0; // encoder count
int old_master_count = 0;

byte arrowLeft[8] = { 0x00,  0x00,  0x04,  0x08,  0x1F,  0x08,  0x04,  0x00 };
byte arrowRight[8] = { 0x0, 0x8, 0xc, 0xe, 0xc, 0x8, 0x0, 0x0 };

byte whichLine = 0;
int currentMenuState = 0;

//These will be the running values;
double setTemp = 75.0;
double setRH = 75.0;
byte setFan = FAN_MODE_OFF;

struct programSetting {
  double temperature;
  double humidity;
  int durationHours;
  byte fanMode; //FAN_MODE_AUTO, FAN_MODE_OFF, or FAN_MODE_ON
};
typedef struct programSetting ProgramSetting;

ProgramSetting _currentProgram;
// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
/*
# D7 = pin 7 = Humidifier
# D9 = pin 9 = Dehumidifier
# D10 = pin 10 = Refrigerator
# D11/D12 = pins 11/12 = encoder
# A1 = AIN2 = = pin 5? or pin 10? = button 1
# A2 = AIN3 = pin 8? = button 2
# A3 = Weight sensor
# A4/A5 = Temp/Humidity sensor
# D2 = fan control
# SCL/SDA = I2C (LCD Display)
*/
/*
A0 = 14
A1 = 15
A2 = 16
A3 = 17
A4 = 18
A5 = 19
*/

SHT1x sht1x(A4, A5); //initialize hum/temp sensor
int val1 = 0, val2 = 0;
int sensorValue = 0;
byte downarrow[8] = {0x0,0x4,0x4,0x4,0x15,0xe,0x4,0x0};

unsigned long lastUpdateTime = 0;
unsigned long lastLogTime = 0;
bool isFridgeOn = false;
bool isHumOn = false;
bool isDehumOn = false;
float temp_f;
float humidity;
     
void setup() {
   _currentProgram.temperature=setTemp;
  _currentProgram.humidity=setRH;
  _currentProgram.fanMode = setFan;
  // Debugging output
  Serial.begin(115200);
  // set up the LCD's number of columns and rows:
  Wire.begin();
  lcd.begin(Wire); //Set up the LCD for I2C communication
  delay(50);

  lcd.disableSystemMessages();
  delay(50);
  //lcd.setFastBacklight(0xFFFFFF); //Set backlight to bright white
  delay(50);
  lcd.setContrast(0x10);
  delay(50);
  lcd.noAutoscroll();
  delay(50);
  lcd.createChar(0, arrowLeft);
  lcd.createChar(1, arrowRight);

  lcd.clear(); //Clear the display - this moves the cursor to home position as well
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);

  if (ENABLED_ENCODER)
  {
    pinMode(ENC_PIN2, INPUT_PULLUP);
    pinMode(ENC_PIN1, INPUT_PULLUP);

    //interrupt channels are same as pin numbers for this board
    attachInterrupt(ENC_PIN2, encoder_ISR_B, CHANGE);
    attachInterrupt(ENC_PIN1, encoder_ISR_A, CHANGE);
  }
  
  //turn the fan on
  if(ENABLED_FAN){
    pinMode(PWR_FAN,OUTPUT);
    digitalWrite(PWR_FAN,1);
  }
  if(ENABLED_PWR){
    pinMode(PWR_FRIDGE,OUTPUT);
    pinMode(PWR_HUM,OUTPUT);
    pinMode(PWR_DEHUM,OUTPUT);    
  }
  //initialize flash
  if (!flash.begin(FLASH_TYPE)) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1);
  }
  //initialize filesystem
  if (!pythonfs.begin()) {
    Serial.println("Failed to mount filesystem!");
    Serial.println("Was CircuitPython loaded on the board first to create the filesystem?");
  }
}

/*
//OLD CODE - SAVING IN CASE I WANT TO LOOK BACK
void pin_ISR_UP() {
  buttonState = 0;
  //digitalWrite(ledPin, buttonState);
}

void pin_ISR_DOWN() {
  unsigned long interrupt_time = millis();
   // If interrupts come faster than 200ms, assume it's a bounce and ignore
   if (interrupt_time - last_interrupt_time > 150) 
   {
      last_interrupt_time = interrupt_time;
      buttonState += 1;
    }
 }
*/

int printLCD(String text){
  if(text.length() > 16){
    return lcd.print(text.substring(0,16));
  }
  else{
    return lcd.print(text);
  }
}

unsigned int lastReportedPos = 1;   // change management
static boolean rotating = false;    // debounce management
boolean A_set = false;
boolean B_set = false;

void encoder_ISR_A() {
   // debounce
  //if ( rotating ) delay (1);  // wait a little until the bouncing is done

  // Test transition, did things really change?
  if ( digitalRead(ENC_PIN1) != A_set ) { // debounce once more
    A_set = !A_set;

    // adjust counter + if A leads B
    if ( A_set && !B_set )
      master_count += 1;

    rotating = false;  // no more debouncing until loop() hits again
  }
}

// Interrupt on B changing state, same as A above
void encoder_ISR_B() {
  //if ( rotating ) delay (1);
  if ( digitalRead(ENC_PIN2) != B_set ) {
    B_set = !B_set;
    //  adjust counter - 1 if B leads A
    if ( B_set && !A_set )
      master_count -= 1;

    rotating = false;
  }
}
volatile bool pressed = false;
void pin_BTN_PRESS()
{

  int val1 = analogRead(A1);
  int val2 = analogRead(A2);
  float volt1 = val1 * (3.3 / 1024.0);
  float volt2 = val2 * (3.3 / 1024.0);
  pressed = true;
  buttonsPressed = 0;
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  //if (interrupt_time - last_interrupt_time > 150)
  //{
  //Serial.println("volt 1: " + String(volt1, 2));
  //Serial.println("volt 2: " + String(volt2, 2));
  //last_interrupt_time = interrupt_time;
  if (1.5 < volt1 && volt1 < 1.8)
  {
    buttonsPressed |= 1UL << 1;
    //Serial.println("pressed 1");
  }
  else if (volt1 > 1.8)
  {
    buttonsPressed |= 1UL << 2;
    //Serial.println("pressed 2");
  }
  //last_interrupt_time = interrupt_time;
  if (1.5f < volt2 && volt2 < 1.8)
  {
    buttonsPressed |= 1UL << 3;
    //Serial.println("pressed 3");
  }
  else if (volt2 > 1.8)
  {
    buttonsPressed |= 1UL << 4;
    //Serial.println("pressed 4");
  }
}

void loop() {
   pin_BTN_PRESS();
  if (Serial && Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.readStringUntil('\n');
      if(incomingByte == "ping"){
        Serial.println("pong");
      }
      else if(incomingByte=="show log") {
        printLogToSerial();
      }
      else if(incomingByte=="fridge") {
        Serial.print("fridge " + isFridgeOn?"On":"Off");
        }
      else if(incomingByte=="fridge on") {
        isFridgeOn = true;
        }
      else if(incomingByte=="fridge off") {
        isFridgeOn = false;
      } else if(incomingByte == "clear log"){
        clearLogFile();
      }

  }
  if (buttonsPressed > 0x0 && lastButtonsPressed != buttonsPressed)
  {
    if ((buttonsPressed >> 1) & 1U) //button up is pressed
      _currentProgram.temperature += 0.5;
    else if ((buttonsPressed >> 2) & 1U) //button down is pressed
      _currentProgram.temperature -= 0.5;
    if ((buttonsPressed >> 4) & 1U) //back button is pressed
    {
      _currentProgram.humidity -= 0.5;
    }
    else if ((buttonsPressed >> 3) & 1U) // enter button is pressed
    {
      _currentProgram.humidity += 0.5;
    }
    last_interrupt_time = millis();
  }
  else if (buttonsPressed > 0 && last_interrupt_time + 800 < millis()) // if we're continuing to hold a button...
  {
    last_interrupt_time = millis();
    if ((buttonsPressed >> 1) & 1U) //button up is pressed
      _currentProgram.temperature += 2.0;
    else if ((buttonsPressed >> 2) & 1U) //button down is pressed
      _currentProgram.temperature -= 2.0;
    if ((buttonsPressed >> 4) & 1U) //back button is pressed
    {
      _currentProgram.humidity -= 2.0;
    }
    else if ((buttonsPressed >> 3) & 1U) // enter button is pressed
    {
      _currentProgram.humidity += 2.0;
    }
  }
    //check temp & humidity, turn stuff on/off if necessary
    if(lastUpdateTime + 5000 < millis()){
        if(ENABLED_TH_SENSOR){
            //float temp_c;
            //temp_c = sht1x.readTemperatureC();
            temp_f = sht1x.readTemperatureF();
            humidity = sht1x.readHumidity();
            if(temp_f <= 0.0 || humidity <= 0.0){
//                lineThree = "TH SNSR OFFLN";
            } else {
                checkTemp(temp_f,_currentProgram.temperature,isFridgeOn);
                checkRH(humidity,_currentProgram.humidity,isHumOn,isDehumOn);
            }
        } else {
//            lineThree = "TH SNSR OFFLN   ";
        }

        lastUpdateTime = millis();
    }
    //display current temp and set temp
    String lineTwo =  "S:  {0}F {1}%";
    String lineOne =  "RT: {0}F {1}%";

    lineTwo.replace("{0}",String(_currentProgram.temperature,1));
    lineTwo.replace("{1}",String(_currentProgram.humidity,1));
    lineOne.replace("{0}",String(temp_f,1));
    lineOne.replace("{1}",String(humidity,1));
    
    /*** WRITE TO LOG FILE ***/
    if((lastLogTime + HALF_MINUTE) <= millis()){
      lastLogTime = millis();
      String logLine = String(millis(),DEC) + ": " + String(_currentProgram.temperature,1) + ", " + String(_currentProgram.humidity,1) + ", " + String(temp_f,1) + ", " + String(humidity,1) + ", ";
      if(isFridgeOn){
        logLine += "ON, ";
      } else {
        logLine += "OFF, ";
      }
      if(isHumOn){
        logLine += "ON";
      } else {
        logLine += "OFF";
      }
      Serial.println(logLine);
      logToFile(logLine);
    }
    if(ENABLED_LCD){
        lcd.setCursor(0, 0);
        //delay(20);
        lcd.print(lineOne); 

        lcd.setCursor(0, 1);
        //delay(20);
        lcd.print(lineTwo); 
    }
}

void checkTemp(float currentTemp,float settemp,bool fridgeOn){
    /*Serial.print("Status ");
    Serial.print(fridgeOn);
    Serial.print("  temp: ");
    Serial.print(currentTemp);
    Serial.print("  ");
    Serial.print("  Set+1.5: ");
    Serial.println(settemp + 1.5);*/
    if(!fridgeOn && currentTemp > settemp + 1.5){
        if(ENABLED_PWR && ENABLED_FRIDGE){
            digitalWrite(PWR_FRIDGE,HIGH);
        }
        isFridgeOn = true;
    } else if(fridgeOn && currentTemp + 1.5 < settemp){
        if(ENABLED_PWR && ENABLED_FRIDGE){
            digitalWrite(PWR_FRIDGE,LOW);
        }
        isFridgeOn = false;
    }
    
}
void checkRH(float currentRH,float setrh,bool humidifierOn, bool dehumidifierOn){
    if(!ENABLED_HUM){
      if(!dehumidifierOn && currentRH > setrh + 2.5){
          if(ENABLED_PWR && ENABLED_DEHUM){
              digitalWrite(PWR_DEHUM,HIGH);
          }
          isDehumOn = true;
      } else if(dehumidifierOn && currentRH + 2.5 <= setrh){
          if(ENABLED_PWR && ENABLED_DEHUM){
              digitalWrite(PWR_DEHUM,LOW);
          }
          isDehumOn = false;
      }
    } else {
      if(!dehumidifierOn && currentRH > setrh + 2.5){
          if(ENABLED_PWR && ENABLED_DEHUM){
              digitalWrite(PWR_DEHUM,HIGH);
          }
          isDehumOn = true;
      } else if(dehumidifierOn && currentRH + 1 <= setrh){
          if(ENABLED_PWR && ENABLED_DEHUM){
              digitalWrite(PWR_DEHUM,LOW);
          }
          isDehumOn = false;
      }
    }
    if(!ENABLED_DEHUM){
      if(!humidifierOn && currentRH + 2.5 <= setrh){
          if(ENABLED_PWR && ENABLED_HUM){
              digitalWrite(PWR_HUM,HIGH);
          }
          isHumOn = true;
      } else if(humidifierOn && currentRH > setrh + 2.5){
          if(ENABLED_PWR && ENABLED_HUM){
              digitalWrite(PWR_HUM,LOW);
          }
          isHumOn = false;
      }
    } else {
      if(!humidifierOn && currentRH + 2.5 <= setrh){
          if(ENABLED_PWR && ENABLED_HUM){
              digitalWrite(PWR_HUM,HIGH);
          }
          isHumOn = true;
      } else if(humidifierOn && currentRH > setrh + 1){
          if(ENABLED_PWR && ENABLED_HUM){
              digitalWrite(PWR_HUM,LOW);
          }
          isHumOn = false;
      }
    }
    if(humidifierOn && dehumidifierOn) { //this should not ever happen, but just putting it here in CASE
        digitalWrite(PWR_HUM,LOW);
        digitalWrite(PWR_DEHUM,LOW);
        isHumOn = false;
        isDehumOn = false;
    }
}

void logToFile(String lineToLog){
  File dataFile = pythonfs.open("system.log", FILE_WRITE);
  // Check that the file opened successfully and write a line to it.
  if (dataFile) {
    dataFile.print(lineToLog);
    dataFile.println();
    // Finally close the file when done writing.  This is smart to do to make
    // sure all the data is written to the file.
    dataFile.close();
  }
}

void clearLogFile(){
  if (!pythonfs.remove("system.log")) {
    Serial.println("Error, couldn't delete system.log file!");
  }
  File dataFile = pythonfs.open("system.log", FILE_WRITE);
  if(dataFile){ dataFile.close(); }
}
void printLogToSerial(){
  File dataFile = pythonfs.open("system.log", FILE_READ);
  if (dataFile) {
     while (dataFile.available()) {
        char c = dataFile.read();
        Serial.print(c);
     }
     Serial.println();
     // Close the file when finished reading.
     dataFile.close();
  } else {
    Serial.println("Failed to open system.log");
  }
  
}
